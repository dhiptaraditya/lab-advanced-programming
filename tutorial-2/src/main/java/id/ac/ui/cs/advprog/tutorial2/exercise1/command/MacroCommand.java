package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands){
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute(){
        // TODO Complete me!
      for (Command command : commands){
        command.execute();
      }
    }

    @Override
    public void undo(){
      int idx = commands.size();
      while(idx > 0){
        Command command = commands.get(idx-1);
        command.undo();
        idx --;
      }
    }
}
