package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void swim(){
      System.out.println("gamau renang");
    }

    public Duck(FlyBehavior a, QuackBehavior b){
      this.flyBehavior = a;
      this.quackBehavior = b;
    }

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }
    public void setFlyBehavior(FlyBehavior flybhv){
      this.flyBehavior = flybhv;
    }
    public void setQuackBehavior(QuackBehavior quackbhv){
      this.quackBehavior = quackbhv;
    }

    // TODO Complete me!
    public abstract void display();
}
