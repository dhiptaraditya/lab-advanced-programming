package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!
  public ModelDuck(FlyBehavior a, QuackBehavior b){
    super(a, b);
  }

  // @Override
  public void display(){
    System.out.println("i'm a model boy duck!!!");
  }
}
