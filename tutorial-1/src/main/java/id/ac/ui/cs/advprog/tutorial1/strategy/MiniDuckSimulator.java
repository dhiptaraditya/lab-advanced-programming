package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public static void main(String[] args) {
        FlyBehavior a = new FlyNoWay();
        QuackBehavior b =  new MuteQuack();

        Duck mallard = new MallardDuck(a, b);
        mallard.performQuack();
        mallard.performFly();

         // TODO: Fix me!
        Duck model = new ModelDuck(a, b);
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
        model.display();        
    }
}
