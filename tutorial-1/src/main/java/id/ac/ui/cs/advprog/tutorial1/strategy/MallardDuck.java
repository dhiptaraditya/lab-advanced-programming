package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!

  public MallardDuck(FlyBehavior a, QuackBehavior b){
    super(a,b);
  }

  // @Override
  public void display(){
    System.out.println("mallard boy duck!!!");
  }
}
