package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    private static Singleton uniqueInstance;

    private Singleton(){}

    public static Singleton getInstance() {
        // to make sure there's only one instance to be made
        if(uniqueInstance == null )uniqueInstance = new Singleton();
        return uniqueInstance;
    }
}
