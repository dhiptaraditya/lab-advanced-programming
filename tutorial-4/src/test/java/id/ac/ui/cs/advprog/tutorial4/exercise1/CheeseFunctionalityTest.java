package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheeseFunctionalityTest {

    private Cheese Cheese;
    private Cheese mozzarellaCheese;
    private Cheese parmesanCheese;
    private Cheese regiannoCheese;

    @Before
    public void setUp() throws Exception {
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        regiannoCheese = new ReggianoCheese();
    }

    @Test
    public void testCheeseOutput(){
        assertEquals("Shredded Mozzarella",mozzarellaCheese.toString());
        assertEquals("Shredded Parmesan",parmesanCheese.toString());
        assertEquals("Reggiano Cheese",regiannoCheese.toString());
    }

}
