package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClamFunctionalityTest {

    private Clams frozenClams;
    private Clams freshClams;

    @Before
    public void setUp() throws Exception {
        freshClams = new FreshClams();
        frozenClams = new FrozenClams();

    }

    @Test
    public void testClamsOutput(){
        assertEquals("Fresh Clams from Long Island Sound",freshClams.toString());
        assertEquals("Frozen Clams from Chesapeake Bay",frozenClams.toString());
    }

}
